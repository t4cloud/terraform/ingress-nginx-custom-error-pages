# Build
FROM golang:1.16.2-buster as BUILD
ENV GO111MODULE=on
RUN mkdir /app/ /tmp/artifacts/
WORKDIR /app
RUN git clone https://github.com/kubernetes/ingress-nginx.git . \
	&& go mod download \
	&& CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags "-s -w" -o /tmp/artifacts/custom-error-pages k8s.io/ingress-nginx/images/custom-error-pages/...

# Image
FROM alpine:3.13
MAINTAINER Ganex <suporte@ganex.com.br>
COPY . /
COPY --from=BUILD /tmp/artifacts/custom-error-pages /
CMD ["/custom-error-pages"]